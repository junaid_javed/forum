$(document).ready(function() {

	// Select2
	$(".select2").select2();

	// Top Rated Doctors
	$('#top-rated').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: false,
		autoplaySpeed: 2000,
		responsive: [
		    {
		      breakpoint: 1199.98,
		      settings: {
		        arrows: true,
		        slidesToShow: 3,
		        slidesToScroll: 3,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        arrows: false,
		        dots: true,
		        slidesToShow: 3,
		        slidesToScroll: 3,
		      }
		    },
		    {
		      breakpoint: 767,
		      settings: {
		        arrows: false,
		        dots: true,
		        slidesToShow: 2,
		        slidesToScroll: 2,
		      }
		    },
		    {
		      breakpoint: 576,
		      settings: {
		        arrows: false,
		        dots: true,
		        slidesToShow: 1,
		        slidesToScroll: 1,
		      }
		    }
		]
	});

	// Testimonials
	$('#testimonials').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: false,
		autoplaySpeed: 2000,
		dots: true,
		responsive: [
		    {
		      breakpoint: 992,
		      settings: {
		        arrows: false,
		        slidesToShow: 1,
		        slidesToScroll: 1,
		      }
		    }
		]
	});

	// Our Partners
	$('#our-partners').slick({
		slidesToShow: 6,
		slidesToScroll: 6,
		autoplay: false,
		autoplaySpeed: 2000,
		responsive: [
		    {
		      breakpoint: 992,
		      settings: {
		        arrows: false,
		        dots: true,
		        slidesToShow: 5,
				slidesToScroll: 5,
		      }
		    },
		    {
		      breakpoint: 767,
		      settings: {
		        arrows: false,
		        dots: true,
		        slidesToShow: 3,
				slidesToScroll: 3,
		      }
		    },
		    {
		      breakpoint: 576,
		      settings: {
		        arrows: false,
		        dots: true,
		        slidesToShow: 2,
				slidesToScroll: 2,
		      }
		    }
		]
	});

	// Show Bootstrap Popover on hover
	$('[data-toggle="popover"]').popover({
        placement : 'bottom',
        trigger : 'hover'
    });

	// Sticky Map 
	$(window).scroll(function(){
	  var sticky = $('.search-map'),
	  scroll = $(window).scrollTop();
	  if (scroll >= 80) sticky.addClass('map-fixed');
	  else sticky.removeClass('map-fixed');
	});


	$('.chatbot').click(function() {
		$('.chat-window').toggleClass('toggle-transform');
		$('.chat-close').toggleClass('toggle-transform');
	});

});
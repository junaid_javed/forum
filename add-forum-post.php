<link rel="stylesheet" href="assets/css/main.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php
require 'database.php';
$title = "";
$body = "";
$image = "";
session_start();

if (isset($_SESSION['user_username'])) {
  if (isset($_GET['edit_id'])) {
    $sql2 = "SELECT * from forum_topic where id=" . $_GET['edit_id'];
    $res = mysqli_query($database, $sql2) or die(mysqli_error($database));
    $fetch = mysqli_fetch_array($res);
    $title = $fetch['forum_topic_name'];
    $body = $fetch['forum_topic_body'];
    // $image = $fetch['forum_topic_image'];
  }
  $user_id = 0;
  if (isset($_SESSION['user_id']))
    $user_id = $_SESSION['user_id'];
  ini_set("display_errors", 1);
  // session_start();
  if (!isset($_FILES['BackgroundImageFile'])) { }
  $user_username = $_SESSION['user_username'];
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require 'database.php';
    $forum_topic_name = $_REQUEST['forum_topic_name'];
    $forum_topic_body = $_REQUEST['forum_topic_body'];
    // $forum_topic_semester = implode(',', $_REQUEST['forum_topic_semester']);

    $Destination = './assets/uploads';
    if (!isset($_FILES['BackgroundImageFile'])) {
      $BackgroundNewImageName = '';
      // move_uploaded_file($_FILES['BackgroundImageFile']['tmp_name'], "/$Destination/$BackgroundNewImageName");
    } else {
      //echo "file present --------------";
      $RandomNum = rand(0, 12345);
      $ImageName = str_replace(' ', '-', strtolower($_FILES['BackgroundImageFile']['name']));
      $ImageType = $_FILES['BackgroundImageFile']['type'];
      $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
      $ImageExt = str_replace('.', '', $ImageExt);
      $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
      $BackgroundNewImageName = $ImageName . '-' . $RandomNum . '.' . $ImageExt;
      $tempString = $Destination . "/" . $BackgroundNewImageName;
      move_uploaded_file($_FILES['BackgroundImageFile']['tmp_name'], $tempString);
    }
    $picture = $_SESSION['picture'];
    echo $picture;
    $sql = "INSERT INTO forum_topic(forum_topic_name, forum_topic_body, forum_topic_time, forum_topic_created_by, forum_topic_image,user_id,picture) VALUES ('$forum_topic_name', '$forum_topic_body', CURRENT_TIMESTAMP, '$user_username' , '$BackgroundNewImageName','$user_id','$picture')";
    mysqli_query($database, $sql) or die(mysqli_error($database));
    header("location:forum.php");
  }
  ?>
<!-- ?php include 'controllers/navigation/first-navigation.php' ? -->
<?php
  // }
  // else{
  ?>
<!-- ?php include 'controllers/navigation/index-before-login-navigation.php' ?-->

<?php
}
?>

<style type="text/css">
.archive_header .section_title {
    text-align: center;
   
    margin: 0;
    padding: 10px 0;
}
.archive_header .section_title {
    font-size: 20px;
}
.section_title {
    background-color: #5e0191;
    color: #ffffff;
}

</style>
<!-- FORUM -->
<section class="add-forum tb-mrgn" action="components/add-new-forum-post.php" method="post" enctype="multipart/form-data" id="UploadForm">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-7">
        <!-- <h1 class="fs-18 fw-600 text-uppercase mb-4 pb-3" style="    color: #5e0191;">Add Forum</h1> -->
        
        <div class="container archive_header page-header" style=" padding-right: 0px; padding-left: 0px;">
            <div class="row section_title">
                <div class="col-md-4">
                        <h1 class="page-title  title" style="float: left"><a style="color:white;" href="forum.php"><i class="fa fa-arrow-left" aria-hidden="true"></a></i></i></h1>
                </div>
                <div class="col-md-4">
                    <?php if (isset($_GET['edit_id'])) { ?>
                        <h1 class="page-title  title" >Edit Post</h1>
                        <?php } else { ?>
                          <h1 class="page-title  title" >Add Post</h1>
                          <?php
                        } ?>
                </div>
                <div class="col-md-4 offset">
                        
                </div>
            </div>
          </div>  
        
        <div class="add-forum-post" style="margin-top: 19px">
          <?php if (isset($_GET['edit_id'])) { ?>
          <form action="edit_post.php" method="post" enctype="multipart/form-data">
            <?php  } else { ?>
            <form action="add-forum-post.php" method="post" enctype="multipart/form-data">
              <?php } ?>
              <div class="form-group">
                <input type="text" class="form-control" id="forum-topic-name" class="form-control" name="forum_topic_name" placeholder="Enter Topic Name" value="<?php echo $title; ?>" required>
              </div>
              <div class="form-group">
                <textarea id="forum-topic-body" rows="9" class="form-control" placeholder="Body of the Topic" name="forum_topic_body" value="" required><?php echo $body; ?></textarea>
              </div>
              <!--             <div class="form-group upload-file">
              <button class="btn form-control">Upload a file</button>
              <input name="BackgroundImageFile" type="file" id="uploadBackgroundFile" class="btn btn-default" name="forum-topic-attachment" value="<?php echo $image; ?>" />
            </div> -->
              <div class="row">
                <a id="title" class="form-control col-md-7" style="height:44px;    margin-left: 16px;    flex: 0 0 63.333333% !important;
    max-width: 63.333333% !important;" placeholder="Title"> </a>
                <div class="form-group upload-file " style="padding-left: 40px;">
                  <input value="choose" style="color:white" class="btn form-control btn-primary gradient-btn read-more " />
                  <input name="BackgroundImageFile" type="file" id="uploadBackgroundFile" class="btn btn-default" name="forum-topic-attachment" required />
                </div>
              </div>
              <div class="row">
                <div class="col-md-8 offset"></div>
                <div class="col-md-4" style="padding-left: 31px;">
                  <div class="form-group">
                    <?php if (isset($_GET['edit_id'])) { ?>
                    <input type='hidden' name='edit_id' value=<?php echo $_GET['edit_id']; ?> />
                    <input type="submit" name="submit_button" id="submit_button" class="btn-primary gradient-btn read-more mt-4" value="Save">
                    <?php  } else { ?>
                    <input type="submit" name="submit_button" id="submit_button" class="btn-primary gradient-btn read-more mt-4" value="Submit">
                    <?php  } ?>
                  </div>
                </div>

              </div>

            </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END FORUM -->

<!-- <script type="text/javascript">
  document.getElementById('uploadBackgroundFile').onchange = function () {

  // var temp = this.value.split('"\"');
  // temp = this.value.replace(/\/[^\/]+\/?$/,'');
  var temp = this.value.replace(String.fromCharCode(92),String.fromCharCode(92,92));
  console.log(temp);
    alert('Selected file: ' + temp);
};
</script> -->
<script type="text/javascript">
  document.getElementById('uploadBackgroundFile').onchange = function() {

    var temp = this.value.replace(String.fromCharCode(92), String.fromCharCode(92, 92));
    temp = temp.split('\\').pop().split('/').pop();
    document.getElementById('title').innerText = temp;
    var temp = this.value.replace(String.fromCharCode(92), String.fromCharCode(92, 92));


  };
</script>
<!-- jQuery Bootstrap JS. -->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/lib/SVGInjector/js/svg-injector.min.js"></script>
<script src="assets/lib/select2/js/select2.full.min.js"></script>
<script src="assets/lib/slick-slider/slick.min.js"></script>
<script src="assets/js/script.js"></script>
<script>
  $('[data-toggle="tooltip"]').tooltip()
</script>
</body>

</html>
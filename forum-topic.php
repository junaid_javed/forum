<link rel="stylesheet" href="assets/css/main.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php
require 'database.php';
// if($_SESSION['user_username']){
session_start();

?>
<!-- ?php include 'controllers/navigation/first-navigation.php' ? -->
<?php
// }
// else{
?>
<!-- ?php include 'controllers/navigation/index-before-login-navigation.php' ?-->

<?php
// }
?>

<?php
// session_start();
$current_user = $_SESSION['user_username'];
if (isset($_GET['add'])) {
  $id = $_GET['add'];
}
if (isset($_GET['id'])) {
  $id = mysqli_real_escape_string($database, $_REQUEST['id']);
}
$update = "UPDATE forum_topic SET forum_topic_views = forum_topic_views+1 WHERE id = '$id'";
mysqli_query($database, $update);

// echo "-------------------".mysqli_error($database);

$sql3 = "SELECT * FROM forum_topic where id=" . $id;
$result2 = mysqli_query($database, $sql3) or die(mysqli_error($database));
$rws = mysqli_fetch_assoc($result2);
$rws_search_username = $rws['forum_topic_created_by'];
// $temp_user_username = $rws['forum_topic_created_by'];
// $sql_search_username = "SELECT * FROM user WHERE user_username = '$temp_user_username'";
// $result_search_username = mysqli_query($database, $sql_search_username) or die(mysqli_error($database));
// $rws_search_username = mysqli_fetch_array($result_search_username);
?>
<style type="text/css">
.archive_header .section_title {
    text-align: center;
   
    margin: 0;
    padding: 10px 0;
}
.archive_header .section_title {
    font-size: 20px;
}
.section_title {
    background-color: #5e0191;
    color: #ffffff;
}

</style>
<!-- FORUM -->
<section class="forum forum-detail tb-mrgn">
  <div class="container">
    <div class="row">



      <div class="col-md-8">
          <div class="container archive_header page-header" style=" padding-right: 0px; padding-left: 0px;">
              <div class="row section_title">
                  <div class="col-md-4">
                          <h1 class="page-title  title" style="float: left"><a style="color:white;" href="forum.php"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></i></h1>
                  </div>
                  <div class="col-md-4">
                          <h1 class="page-title  title" >Forum Detail</h1>
                  </div>
                  <div class="col-md-4 offset">
                          
                  </div>
              </div>
            </div> 
        <!-- <div class="forum-header d-flex justify-content-between align-items-center mb-4">
          <h1 class="fs-18 fw-600 text-uppercase">Forum Detail</h1>
        </div> -->
        <div class="forum-block bg-white d-flex justify-content-between" style="margin-top: 10px">
          <div class="forum-block-leftbar">
            <div class="forum-block-img">
              <img class="rounded-circle" src="<?php echo $rws['picture']; ?>" class="img-responsive" alt="<?php echo $rws_search_username; ?>">
            </div>
          </div>
          <div class="forum-block-middlebar col-md-11">
            <div class="forum-block-middlebar-title fs-16 fw-500 text-uppercase mt-1"><?php echo $rws['forum_topic_name']; ?></div>
            <div class="forum-block-rightbar d-flex text-center align-items-center mb-2 mt-2">
              <div class="forum-block-username fs-14 fw-400">By: <a href="#"> <?php echo $rws['forum_topic_created_by']; ?> </a></div>
              <div class="forum-block-comments-views fs-12 fw-400 ml-3 mr-3">
                <img src="assets/images/eye.png" alt="Views">
                <span><?php echo $rws['forum_topic_views'] ?></span>
              </div>
              <div class="forum-block-comments-time fs-12 fw-400">
                <img src="assets/images/clock-circular.png" alt="Time">
                <span> <?php echo $rws['forum_topic_time']; ?> </span>
              </div>
            </div>
            <p class="col-md-12 fs-14 fw-300 mt-2" style="    -webkit-box-orient: vertical !important;
    overflow: hidden !important;
    display: -webkit-box !important;
    -webkit-line-clamp: 3 !important;
    color: rgba(76, 76, 76, .8);
    font-size: 15px !important;
    font-weight: 300 !important;
    word-break: break-all;" title="<?php echo $rws['forum_topic_body']; ?>"> <?php echo $rws['forum_topic_body']; ?></p>
            <div class="forum-attachment">
              <?php
              if ($rws['forum_topic_image']) {
                ?>
              <hr style="margin:0;">
              <div class="col-md-3 column">
                <img src="assets/uploads/<?php echo $rws['forum_topic_image']; ?>" class="img-responsive thumbnail mt-3" style="width: 100%;height: 100px;">
              </div>
              <?php
              }
              ?>
            </div>
          </div>
        </div>

        <h2 class="fs-18 fw-600 text-uppercase mt-5" style="color: #5e0191;">All Comments</h2>

        <?php
        $sql_reply = "SELECT * FROM forum_topic_reply where forum_topic_reply_topic_id = '$id'";
        $result_reply = mysqli_query($database, $sql_reply);
        $rws_reply_count = mysqli_num_rows($result_reply);
        while ($rws_reply = mysqli_fetch_array($result_reply)) {

          // $temp_user_username_reply = $rws_reply['forum_topic_reply_created_by'];
          // $sql_search_username_reply = "SELECT * FROM user WHERE user_username = '$temp_user_username_reply'";
          // $result_search_username_reply = mysqli_query($database,$sql_search_username_reply);
          // $rws_search_username_reply = mysqli_fetch_array($result_search_username_reply);
          ?>
        <?php
          if ($rws_reply_count > 0) {
            ?>
        <ul class="forum-detail-comments mt-4">
          <li class="d-flex bg-white">
            <div class="detail-comments-img pr-3">
              <img class="rounded-circle" src="<?php echo $rws_reply['picture']; ?>" alt="Doctor">
            </div>
            <div class="detail-comment-meta">
              <div class="detail-comment-user fs-14 fw-400">
                <a href="#"><?php echo $rws_reply['name']; ?> </a>
              </div>
              <div class="detail-comment-dec fs-14 fw-300 mt-1 pb-2 mb-3" title="<?php echo $rws_reply['forum_topic_reply_body']; ?>"> <?php echo $rws_reply['forum_topic_reply_body']; ?></div>
              <div class="detail-comment-attachment">
                <?php
                    if ($rws_reply['forum_topic_reply_image']) {
                      ?>
                <img class="rounded" src="assets/uploads/<?php echo $rws_reply['forum_topic_reply_image']; ?>" alt="Doctor">
                <?php
                    }
                    ?>
              </div>

            </div>
          </li>

        </ul>
        <?php
          }
        }
        ?>
        <?php
        // session_start();
        if (isset($_SESSION['user_username'])) {
          ?>

        <h3 class="fs-18 fw-600 text-uppercase mt-5" style="color: #5e0191;">Post Your Comment</h3>
        <div class="add-forum-post mt-4">
          <form action="components/forum-topic-reply.php?id=<?php echo $rws['id']; ?>" method="post" enctype="multipart/form-data" id="UploadForm">
            <div class="form-group">
              <textarea id="forum-topic-reply-body" rows="4" class="form-control" placeholder="Body of the Topic" name="forum_topic_reply_body" required></textarea>
            </div>
            <!-- <div class="form-group upload-file">
              <button class="btn form-control">Upload an Attachment</button>
              <input  name="BackgroundImageFile" type="file" id="uploadBackgroundFile" class="btn btn-default" name="forum-topic-attachment" />
            </div> -->
            <div class="row">
              <a id="title" class="form-control col-md-7" style="height:44px;    margin-left: 16px;        flex: 0 0 67.5% !important;
    max-width: 67.5% !important;" placeholder="Title"> </a>
              <div class="form-group upload-file " style="padding-left: 40px;">
                <input value="choose" style="color:white" class="btn form-control btn-primary gradient-btn read-more " />
                <input name="BackgroundImageFile" type="file" id="uploadBackgroundFile" class="btn btn-default" name="forum-topic-attachment" />
              </div>
            </div>
            <div class="row">
              <div class="col-md-9 offset" style="flex: 0 0 74.5% !important;
    max-width: 74.5% !important;"></div>
              <div class="col-md-3" style="    padding-left: 0;">
                <div class="form-group">
                  <input type="submit" name="submit_button" id="submit_button" class="btn-primary gradient-btn read-more mt-4" value="Submit">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-4">
        <div class="forum-sidebar bg-white">
          <h2 class="fs-18 fw-600 text-uppercase" style="    color: #5e0191;">Popular Forums</h2>
          <ul class="popular-forums-list f-14 fw-300">
            <?php
              // session_start();
              $sql = "SELECT * FROM forum_topic ORDER BY forum_topic_views DESC Limit 5";
              $result1 = mysqli_query($database, $sql);
              while ($rws1 = mysqli_fetch_array($result1)) {

                // $temp_user_username = $rws1['forum_topic_created_by'];
                // $sql_search_username = "SELECT * FROM user WHERE user_username = '$temp_user_username'";
                // $result_search_username = mysqli_query($database,$sql_search_username) or die(mysqli_error($database));
                // $rws_search_username = mysqli_fetch_array($result_search_username);

                ?>
            <li>
              <a href="forum-topic.php?id=<?php echo $rws1['id']; ?>" style="    color: #5e0191;"><i class="fa fa-link" style="    color: #5e0191;"></i> <?php echo $rws1['forum_topic_name']; ?></a>
              <br>
              <a href="#" title="Lorem ipsum dolor sit amet" class="pl-4" style="font-size: 13px;color:rgba(76, 76, 76, .8);"><?php echo $rws1['forum_topic_created_by']; ?></a>
            </li>

            <?php
              }
              ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END FORUM -->
<?php
} else {
  ?>
<div class="well">
  <div class="row clearfix">
    <div class="col-md-12 column">
      <h3 class="text-center">You need to <a href="login.php">Log In</a> or <a href="register.php">Register</a> to post comments.</h3>
    </div>
  </div>
</div>
<?php
}
?>
<script type="text/javascript">
  document.getElementById('uploadBackgroundFile').onchange = function() {

    var temp = this.value.replace(String.fromCharCode(92), String.fromCharCode(92, 92));
    temp = temp.split('\\').pop().split('/').pop();
    document.getElementById('title').innerText = temp;
    var temp = this.value.replace(String.fromCharCode(92), String.fromCharCode(92, 92));


  };
</script>
<!-- jQuery Bootstrap JS. -->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/lib/SVGInjector/js/svg-injector.min.js"></script>
<script src="assets/lib/select2/js/select2.full.min.js"></script>
<script src="assets/lib/slick-slider/slick.min.js"></script>
<script src="assets/js/script.js"></script>
<script>
  $('[data-toggle="tooltip"]').tooltip()
</script>
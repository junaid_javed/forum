<?php
    session_start();
    require '../database.php';
    session_destroy();
    header('location:../login.php?logout=success');
?>
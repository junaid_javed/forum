<link rel="stylesheet" href="assets/css/main.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<?php
require 'database.php';
session_start();
$active_page;
if (isset($_SESSION['user_username'])) {
  if (isset($_GET['delete_id'])) {
    $sql2 = "DELETE from forum_topic where id =" . $_GET['delete_id'];

    $res_data2 = mysqli_query($database, $sql2);
    if (!$res_data2) {
      echo mysqli_error($database);
    }
  }
  ?>
<!-- ?php include 'controllers/navigation/first-navigation.php' ? -->
<?php
  // }
  // else{
  ?>
<!-- ?php include 'controllers/navigation/index-before-login-navigation.php' ?-->

<?php
}

function humanTiming($time)
{

  $time = time() - $time; // to get the time since that moment
  $time = ($time < 1) ? 1 : $time;
  $tokens = array(
    31536000 => 'year',
    2592000 => 'month',
    604800 => 'week',
    86400 => 'day',
    3600 => 'hour',
    60 => 'minute',
    1 => 'second'
  );

  foreach ($tokens as $unit => $text) {
    if ($time < $unit) continue;
    $numberOfUnits = floor($time / $unit);
    return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
  }
}
?>



<!-- get forums topics from DB -->
<?php
if (isset($_POST['search'])) {
  if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
  } else {
    $pageno = 1;
  }
  $no_of_records_per_page = 10;
  $offset = ($pageno - 1) * $no_of_records_per_page;
  $total_pages_sql = "SELECT COUNT(*) FROM forum_topic where forum_topic_name like '%" . $_POST['search'] . "%'" or die(mysqli_error($database));
  $result = mysqli_query($database, $total_pages_sql);
  if ($result) {
    $total_rows = mysqli_fetch_array($result)[0];
    $total_pages = ceil($total_rows / $no_of_records_per_page);
    $sql = "SELECT * FROM forum_topic where forum_topic_name like '%" . $_POST['search'] . "%'" or die(mysqli_error($database));
    $res_data = mysqli_query($database, $sql);
  }
  // echo $total_pages_sql;
} else {
  if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
  } else {
    $pageno = 1;
  }
  $no_of_records_per_page = 10;
  $offset = ($pageno - 1) * $no_of_records_per_page;


  $total_pages_sql = "SELECT COUNT(*) FROM forum_topic";
  $result = mysqli_query($database, $total_pages_sql);
  if ($result) {
    $total_rows = mysqli_fetch_array($result)[0];
    $total_pages = ceil($total_rows / $no_of_records_per_page);
    $sql = "SELECT * FROM forum_topic ORDER BY forum_topic_time DESC LIMIT $offset, $no_of_records_per_page";
    $res_data = mysqli_query($database, $sql);
  }
}
?>


<style type="text/css">
  .pagination a {
    color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
  }

  .pagination a.active {
    background-color: #5e0191 !important;
    color: white;

  }

  .pagination a:hover:not(.active) {
    background-color: #ddd;
  }
  .archive_header .section_title {
    text-align: center;
    
    margin: 0;
    padding: 10px 0;
}
.archive_header .section_title {
    font-size: 20px;
}
.section_title {
    background-color: #5e0191;
    color: #ffffff;
}

</style>


<!-- FORUM -->
<section class="forum tb-mrgn">
  <div class="container">
    <div class="row">
      <div class="col-md-8" style="bottom:23px">
        <!-- <div class="forum-header d-flex justify-content-between align-items-center mb-4">
          <h1 class="fs-18 fw-600 text-uppercase" style="    color: #5e0191;">All Forum Posts</h1>
         
        </div> -->
        <div class="container archive_header page-header" style=" padding-right: 0px; padding-left: 0px;">
            <div class="row section_title">
                <div class="col-md-4">
                        <h1 class="page-title  title" style="float: left" [routerLink]="['/blog']"></h1>
                </div>
                <div class="col-md-4">
                        <h1 class="page-title  title" >All Forum Posts</h1>
                </div>
                <div class="col-md-4 offset">
                        
                </div>
            </div>
          </div>     

        <div class="list-paginate-wrapper" style="margin-top: 11px">
          <?php
          if ($result) {
            while ($rws = mysqli_fetch_array($res_data)) {
              //here goes the data
              // $current_user = $_SESSION['user_username'];

              // $temp_user_username = $rws['forum_topic_created_by'];
              // $sql_search_username = "SELECT * FROM user WHERE user_username = '$temp_user_username'";
              // $result_search_username = mysqli_query($database,$sql_search_username) or die(mysqli_error($database));
              // $rws_search_username = mysqli_fetch_array($result_search_username);
              ?>
          <div class="list-paginate-item forum-block bg-white d-flex justify-content-between">

            <div class="forum-block-leftbar col-md-1" style="flex: 0 0 8.666667% !important; max-width: 8.666667% !important;">
              <div class="forum-block-img">
                <img class="rounded-circle" src="<?php echo $rws['picture']; ?>" alt="<?php echo $_SESSION['user_username']; ?>">
              </div>
              <!--        <div class="forum-block-leftbar-btns d-flex justify-content-center mt-2">
                  <a href="#" class="btn-edit btn-success" title="Edit" data-toggle="tooltip" data-placement="top">
                    <img src="assets/images/edit-icon.png" alt="Edit">
                  </a>
                  <a href="#" class="btn-delete" title="Delete" data-toggle="tooltip" data-placement="top">
                    <img src="assets/images/delete-icon.png" alt="Delete">
                  </a>
                </div> -->
              <!--          <?php if ($rws['user_id'] == $_SESSION["user_id"]) { ?>
              <div class="row">
                <span class="col-md-12" style="padding-left: 17px;top: 8px;    display: inherit;">
                  <a href=<?php echo "add-forum-post.php?edit_id=" . $rws['id'] ?>>

                    <img style="display: inline;padding-right: 10px;" alt="Edit"
                      src="assets/images/edit-gradient-icon.png" (click)="editPackage(app.id)">
                  </a>
                  <a href=<?php echo "forum.php?delete_id=" . $rws['id']; ?>>
                    <img style="display: inline" src="assets/images/delete-gradient-icon.png" alt="Delete"
                      (click)="deletePackage(app.id)">
                  </a>
                </span>
              </div>
              <?php } ?> -->
            </div>
            <div class="forum-block-middlebar col-md-9">
              <div class="forum-block-middlebar-title fs-16 fw-500 text-uppercase mt-1">
                <a href="forum-topic.php?add=<?php echo $rws['id']; ?>"><?php echo $rws['forum_topic_name']; ?></a>
              </div>
              <p class="fs-14 fw-300 mt-2" style="-webkit-box-orient: vertical !important;
                overflow: hidden !important;
                display: -webkit-box !important;
                -webkit-line-clamp: 3 !important;
                color: rgba(76, 76, 76, .8);
                font-size: 15px !important;
                font-weight: 300 !important;
      word-break: break-all" title="<?php echo $rws['forum_topic_body']; ?>"><?php echo $rws['forum_topic_body']; ?></p>
            </div>
            <div class="row">
              <div class="col-md-8">

              </div>
            </div>
            <div class="forum-block-rightbar text-center">
              <!-- <div class="forum-block-comments fs-14 fw-400">200</div> -->
              <?php if ($rws['user_id'] == $_SESSION["user_id"]) { ?>
              <div class="row">
                <span class="col-md-12" style="padding-left: 37px;bottom:5px;    display: inherit;">
                  <a href=<?php echo "add-forum-post.php?edit_id=" . $rws['id'] ?>>

                    <img style="display: inline;padding-right: 10px;" alt="Edit" src="assets/images/edit-gradient-icon.png" (click)="editPackage(app.id)">
                  </a>
                  <a href=<?php echo "forum.php?delete_id=" . $rws['id']; ?>>
                    <img style="display: inline" src="assets/images/delete-gradient-icon.png" alt="Delete" (click)="deletePackage(app.id)">
                  </a>
                </span>
              </div>
              <?php } ?>
              <div class="forum-block-comments-views fs-12 fw-400">
                <img src="assets/images/eye.png" alt="Views">
                <span><?php echo $rws['forum_topic_views']; ?></span>
              </div>
              <?php $time = strtotime($rws['forum_topic_time']);
                  $converted = humanTiming($time);
                  ?>
              <div class="forum-block-comments-time fs-12 fw-400">
                <img src="assets/images/clock-circular.png" alt="Time">
                <span> <?php echo $converted; ?></span>
              </div>
            </div>


          </div>
          <?php
            }
          } else {
            echo "<h2>No record found</h2>";
          }
          ?>
        </div>

        <!-- <div id="pagination-container"></div> -->
        <div class="pagination" style="    margin-top: 20px;">
          <a href="<?php if ($pageno <= 1) {
                      echo '#';
                    } else {
                      echo "?pageno=" . ($pageno - 1);
                    } ?>">&laquo;</a>

          <?php
          if (isset($_GET['pageno'])) {
            $active_page = $_GET['pageno'];
          } else {
            $active_page = 1;
          }
          for ($count = 1; $count <= $total_pages; $count++) {
            ?>
          <a class='<?php if ($active_page == $count) {
                        echo "active";
                      } ?>' href=<?php echo "?pageno=" . $count; ?>><?php echo $count; ?></a>
          <?php } ?>
          <a href="<?php if ($pageno >= $total_pages) { } else {
                      echo "?pageno=" . ($pageno + 1);
                    } ?>">&raquo;</a>
        </div>
        <ul class="pagination" style="    padding-left: 12px;" hidden>
          <li><a href="?pageno=1">First</a></li>
          <li class="<?php if ($pageno <= 1) {
                        echo 'disabled';
                      } ?>">
            <a href="<?php if ($pageno <= 1) { } else {
                        echo "?pageno=" . ($pageno - 1);
                      } ?>">Prev</a>
          </li>
          <li class="<?php if ($pageno >= $total_pages) {
                        echo 'disabled';
                      } ?>">
            <a href="<?php if ($pageno >= $total_pages) {
                        echo '';
                      } else {
                        echo "?pageno=" . ($pageno + 1);
                      } ?>">Next</a>
          </li>
          <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
        </ul>
      </div>




      <div class="col-md-4" style="margin-top: 18px;">
         <div class="row">
        <div class="col-md-4 offset" style="    flex: 0 0 51.333333% !important;
        max-width: 51.333333% !important;"></div>
        <div class="col-md-4">
        <a href="add-forum-post.php" class="btn-primary gradient-btn read-more col-md-12" title="Add Forum Post">Add Post</a>
      </div>
    </div>
          <div class="form-group col-md-12" style="    margin-top: 9px; padding-left: 0">

              <form class="example" action="forum.php" method="post">
                <input type="text" placeholder="Search.." name="search" style="float: left" class="col-md-9">
                <button class="btn" type="submit" style="    width: 43px; height: 30px !important;background-color: #5e0191;color: white;border-radius: 1%"><i class="fa fa-search"></i></button>
              </form>
              <br>
              <div id="display" style="display: none;"></div>
            </div>
        <div class="forum-sidebar bg-white col-md-10">
          <h2 class="fs-18 fw-600 text-uppercase" style=" color: #5e0191;">Popular Forums</h2>
          <ul class="popular-forums-list f-14 fw-300">
            <?php
            // session_start();
            $sql = "SELECT * FROM forum_topic ORDER BY forum_topic_views DESC Limit 5";
            $result1 = mysqli_query($database, $sql);
            while ($rws1 = mysqli_fetch_array($result1)) {
              $temp_user_username = $rws1['forum_topic_created_by'];
              // $sql_search_username = "SELECT * FROM user WHERE user_username = '$temp_user_username'";
              // $result_search_username = mysqli_query($database,$sql_search_username) or die(mysqli_error($database));
              // $rws_search_username = mysqli_fetch_array($result_search_username);

              ?>
            <li>
              <a href="forum-topic.php?id=<?php echo $rws1['id']; ?>" style="    color: #5e0191;"><i class="fa fa-link" style="    color: #5e0191;"></i>
                <?php echo $rws1['forum_topic_name']; ?></a>
              <br>
              <a href="#" title="Lorem ipsum dolor sit amet" class="pl-4" style="font-size: 13px;color: rgba(76, 76, 76, .8);"><?php echo $temp_user_username; ?></a>
            </li>

            <?php
            }
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END FORUM -->



<!-- jQuery Bootstrap JS. -->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/lib/SVGInjector/js/svg-injector.min.js"></script>
<script src="assets/lib/select2/js/select2.full.min.js"></script>
<script src="assets/lib/slick-slider/slick.min.js"></script>
<script src="assets/js/script.js"></script>
<!-- <script src="assets/js/paginate.js"></script> -->
<script>
  $('[data-toggle="tooltip"]').tooltip()
</script>


<script>
  // jQuery Plugin: http://flaviusmatis.github.io/simplePagination.js/

  paginate();

  function paginate() {
    var items = $(".list-paginate-wrapper .list-paginate-item");
    var numItems = items.length;
    var perPage = 1;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
      items: numItems,
      itemsOnPage: perPage,
      prevText: "&laquo;",
      nextText: "&raquo;",
      onPageClick: function(pageNumber) {
        var showFrom = perPage * (pageNumber - 1);
        var showTo = showFrom + perPage;
        items.hide().slice(showFrom, showTo).show();
      }
    });
  }
</script>